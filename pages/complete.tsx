import { CheckRounded, ContentCopyRounded } from "@mui/icons-material";
import {
    Container,
    Typography,
    Grid,
    TextField,
    Fab,
    Box,
} from "@mui/material";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import ReactQRCode from "react-qr-code";
import QRCode from "qrcode";

const Shorten: NextPage = () => {
    const router = useRouter();
    const { id } = router.query;
    const fullPath = `${process.env.NEXT_PUBLIC_SERVICE_URL_PREFIX}/${id}`;
    const fullPathWithProtocol = `${process.env.NEXT_PUBLIC_SERVICE_PROTOCAL}${fullPath}`;
    const [copied, setCopied] = useState(false);
    const [copiedQR, setCopiedQR] = useState(false);
    const copy = async () => {
        await navigator.clipboard.writeText(fullPathWithProtocol);
        setCopied(true);
    };
    const copyQR = async () => {
        const canvas: HTMLCanvasElement = await new Promise((r) =>
            QRCode.toCanvas(fullPathWithProtocol, (e, b) => r(b))
        );
        const blob: Blob | null = await new Promise((r) =>
            canvas.toBlob(r, "image/png")
        );
        if (!blob) return;
        navigator.clipboard.write([new ClipboardItem({ "image/png": blob })]);
        setCopiedQR(true);
    };
    const goHome = () => {
        router.push(`/`);
    };
    return (
        <Container
            className="Complete"
            maxWidth="md"
            sx={{
                width: "100%",
                height: "100%",
                display: "flex",
                alignItems: "center",
            }}
        >
            <Grid container spacing={3} sx={{ transform: "translateY(0)" }}>
                <Grid item xs={12}>
                    <Typography variant="h6">
                        {process.env.NEXT_PUBLIC_SERVICE_NAME} has shortened
                        your URL 🎉 🎉 🎉
                    </Typography>
                </Grid>
                <Grid item xs={10}>
                    <TextField
                        variant="outlined"
                        label="Shortend URL"
                        value={fullPath}
                        fullWidth
                    ></TextField>
                </Grid>
                <Grid item xs={2}>
                    <Fab
                        size="large"
                        variant="extended"
                        color="primary"
                        aria-label="copy"
                        onClick={() => copy()}
                    >
                        {!copied ? (
                            <>
                                <ContentCopyRounded />
                                Copy
                            </>
                        ) : (
                            <>
                                <CheckRounded />
                                Copied
                            </>
                        )}
                    </Fab>
                </Grid>

                <Grid item xs={11}>
                    <Box sx={{ display: "flex", justifyContent: "center" }}>
                        <ReactQRCode value={fullPathWithProtocol} />
                    </Box>
                </Grid>

                <Grid
                    item
                    xs={11}
                    sx={{ display: "flex", justifyContent: "center" }}
                >
                    <Fab
                        size="small"
                        variant="extended"
                        color="primary"
                        aria-label="copy qr"
                        onClick={() => copyQR()}
                    >
                        {!copiedQR ? (
                            <>
                                <ContentCopyRounded />
                                Copy QR
                            </>
                        ) : (
                            <>
                                <CheckRounded />
                                Copied QR
                            </>
                        )}
                    </Fab>
                </Grid>
                <Grid
                    item
                    xs={11}
                    sx={{ display: "flex", justifyContent: "center" }}
                >
                    <Fab
                        size="medium"
                        variant="extended"
                        color="secondary"
                        aria-label="copy qr"
                        onClick={goHome}
                    >
                        Make Another One
                    </Fab>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Shorten;
