import type { NextApiRequest, NextApiResponse } from "next";
import { prisma } from "../../ultility/prisma";
import { nanoid } from "nanoid";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { destinationURL } = req.body;
    console.log(destinationURL);

    if (!destinationURL) return res.status(400);

    const id = nanoid(5);

    const modifiedDestinationURL = /^https?:\/\//.test(destinationURL)
        ? destinationURL
        : `http://${destinationURL}`;

    const shortURL = await prisma.shortURL.upsert({
        where: { destinationURL: modifiedDestinationURL },
        update: {},
        create: {
            id,
            destinationURL: modifiedDestinationURL,
        },
    });

    return res.status(200).json(shortURL);
}
