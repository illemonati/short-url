import type { NextApiRequest, NextApiResponse } from "next";
import { prisma } from "../../../ultility/prisma";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { id } = req.query;
    const ip = req.socket.remoteAddress;
    const forwardedForIp = req.headers["x-forwarded-for"];
    const interpretedIp = (forwardedForIp as string) || ip;
    const shortURL = await prisma.shortURL.update({
        select: {
            destinationURL: true,
        },
        where: {
            id: id as string,
        },
        data: {
            visits: {
                increment: 1,
            },
            visitEntries: {
                create: [
                    {
                        ip,
                        host: req.headers.host,
                        forwardedFor: req.headers.forwarded,
                        userAgent: req.headers["user-agent"],
                        headers: req.headers,
                        rawHeaders: req.rawHeaders,
                        interpretedIp,
                    },
                ],
            },
        },
    });
    if (!shortURL) {
        res.redirect("/");
    } else {
        res.redirect(shortURL.destinationURL);
    }
}
