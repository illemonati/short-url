import type { NextApiRequest, NextApiResponse } from "next";
import { prisma } from "../../../ultility/prisma";

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse
) {
    const { id } = req.query;
    const shortURL = await prisma.shortURL.findUnique({
        where: {
            id: id as string,
        },
    });
    if (!shortURL) {
        res.status(400).json({ info: null });
    } else {
        res.status(200).json({ info: shortURL });
    }
}
