import { ArrowDropDown, ArrowForwardRounded } from "@mui/icons-material";
import {
    Container,
    Box,
    Typography,
    Grid,
    TextField,
    Fab,
} from "@mui/material";
import axios from "axios";
import type { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect } from "react";

const Shorten: NextPage = () => {
    const router = useRouter();
    const { u: base64DestinationURL } = router.query;
    useEffect(() => {
        (async () => {
            if (!base64DestinationURL) return;
            const resp = await axios.post(`/api/shorten`, {
                destinationURL: window.atob(base64DestinationURL as string),
            });
            const { id } = resp.data;
            router.push(`/complete?id=${id}`);
        })();
    }, [base64DestinationURL]);
    return (
        <Container
            className="Shorten"
            maxWidth="md"
            sx={{
                width: "100%",
                height: "100%",
                display: "flex",
                alignItems: "center",
            }}
        >
            <Grid container spacing={3} sx={{ transform: "translateY(-40%)" }}>
                <Grid item xs={12}>
                    <Typography variant="h6">
                        {process.env.NEXT_PUBLIC_SERVICE_NAME} is working hard
                        to shorten your URL
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Shorten;
