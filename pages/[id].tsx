import { GetServerSideProps, NextPage } from "next";

const IDRedirectPage: NextPage = () => {
    return <div></div>;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
    const { id } = context.params!;
    console.log(`redirection to ${id}`);
    return {
        redirect: {
            permanent: false,
            destination: `/api/goto/${id}`,
        },
    };
};

export default IDRedirectPage;
