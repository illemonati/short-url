import { ArrowDropDown, ArrowForwardRounded } from "@mui/icons-material";
import {
    Container,
    Box,
    Typography,
    Grid,
    TextField,
    Fab,
} from "@mui/material";
import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import router from "next/router";
import { useState } from "react";

const Home: NextPage = () => {
    const [destinationURL, setDestinationURL] = useState("");

    const shorten = () => {
        const base64DestionationURL = window.btoa(destinationURL);
        router.push(`/shorten?u=${base64DestionationURL}`);
    };

    return (
        <Container
            className="HomePage"
            maxWidth="md"
            sx={{
                width: "100%",
                height: "100%",
                display: "flex",
                alignItems: "center",
            }}
        >
            <Grid container spacing={3} sx={{ transform: "translateY(-40%)" }}>
                <Grid item xs={12}>
                    <Typography variant="h6">
                        {process.env.NEXT_PUBLIC_SERVICE_NAME} URL Shortener
                    </Typography>
                </Grid>
                <Grid item xs={9}>
                    <TextField
                        variant="outlined"
                        label="Destination URL"
                        placeholder="https://www.wikipedia.com/"
                        value={destinationURL}
                        onChange={(e) =>
                            setDestinationURL(() => e.target.value)
                        }
                        onKeyUp={(e) => e.key === "Enter" && shorten()}
                        fullWidth
                    ></TextField>
                </Grid>
                <Grid item xs={1}>
                    <Fab
                        size="medium"
                        color="primary"
                        aria-label="shorten"
                        onClick={() => shorten()}
                    >
                        <ArrowForwardRounded />
                    </Fab>
                </Grid>
                <Grid item xs={2}>
                    <Fab
                        // size="medium"
                        variant="extended"
                        color="secondary"
                        aria-label="more options"
                    >
                        <ArrowDropDown />
                        More
                    </Fab>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="caption">
                        P.S. There are intentionally no security measures on
                        this service. Feature not Bug ;)
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
};

export default Home;
