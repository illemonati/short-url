/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    async redirects() {
        return [
            // {
            //     source: "/:id",
            //     destination: "/api/goto/:id",
            //     permanent: false,
            // },
            {
                source: "/favicon.ico",
                destination: "x",
                permanent: false,
            },
        ];
    },
};

module.exports = nextConfig;
